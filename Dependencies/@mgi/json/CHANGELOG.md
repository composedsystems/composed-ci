## 0.1.7

Added support for 2D and 3D arrays.

## 0.1.6

Updated Readme and Relinked VIs

## 0.1.5

Tweaked access scope and VI properties to solve broken build issues.

## 0.1.4

Fixed bad links in gpackage.json

## 0.1.3

Fixed bug when converting a timestamp to an ISO8601 string in non-negative time zones.

## 0.1.2

Removed UTF converter when on RT.

## 0.1.1

Fixed NaN/Inf case for numbers.

## 0.1.0

Initial Release.
