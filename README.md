# composed-ci

Composed-CI takes the LabVIEW-specific configuration complexities out of your continuous integration. 

This package exists because of the following premises:
1. A LabVIEW project should know how to test and build itself. 
2. Configuration and testing of the LabVIEW-specific test and build steps should be able to be performed in the LabVIEW IDE with an equivalent command line API available for the CI server.

By managing LabVIEW&#39;s test-and-build configuration in the project, the API and number of parameters the CI server needs to manage are minimized, which decouples projects from the CI server.  This assists with configuring the CI server by removing responsibilities, and minimizes the amount of information that the CI server has to know about your project.

Test-and-build parameters are stored in a json configuration file that lives next to your source code.
Includes a user interface to select which build specifications and tests to run.  


# Installation 
GPM provides a copy per project.  Composed-CI can now also be installed as a NI Package.  
The latest package can be found here:  
https://bitbucket.org/composedsystems/composed-ci/downloads/

The package will install the source in the Public Documents directory:  
i.e. C:\Users\Public\Documents\LabVIEW Data\Composed-CI\

# Dependencies

This project uses VI Analyzer and JKI VI Tester.

This package depends on the following VI Packages (all available on VIPM):
LabVIEW CLI by Wiresmith Technology
JKI_labs_tool_vi_tester by JKI
VI Tester JUnit XML Test Results by JKI
VI Analyzer Results to Checkstyle XML Format by National Instruments

This package depends on the following NI Products:
VI Analyzer

This package depends on Git (Git must be installed and accessable from the command line)

# Command Details
1.	**Build**
This runs one or more LabVIEW build specifications and zips the output.
2.	**Mass Compile**
This performs a LabVIEW native mass compile on the working directory
3.	**Open Project**
This opens the project and checks for broken vis.  It also checks for dependency loops.
4.	**VI Analyzer**
Runs one or more VIA configuration files
5.	**VI Tester**
Runs one or more VI Tester test cases.  Tests can be individually whitelisted (only run these) or blacklisted.  Leave blank to run all.
6.	**Relink**
This will relink G packages in a temporary project.  This feature is now a part of GPM and will be deprecated.
7.	**Recompile**
This feature does a bottom up forced recompile of all items in your project.  This fixes the majority of linking issues when relink and mass compile fail.  This feature is also included in GPM and may be deprecated.
8.	**Import Externals** (beta)
This will clone any git URL into an externals directory in your project. If it is a g package, its dependencies will be added to the parent project.  Does not relink or recompile, this should be done manually based on the project's current situation.

# Source code details and contribution guidelines

The project is composed of several distinct layers with different responsibilities.  If you would like to contribute, please conform to this structure for new capabilities.
1.	**CLI API**

The top level API to be invoked via command line.  This layer is responsible for writing the command line output, parsing the command line arguments, and invoking the manual api.

2.	**Manual API**

The topmost level API that can be called with LabVIEW open (CLI requires LabVIEW to be closed before running).  This layer is called from the configure build.vi.  This layer is responsible for input validation and calling the composed-ci class methods.

3.	**Composed-ci.lvclass**

This class�s purpose is to parse composed-ci.json and make the parameters available to the build steps.
a.	**Build Steps**
The build steps unbundle the relevant data from the class private data, validate paths, and call the minimal steps, and report the results to the caller.
b.	**Minimal steps**
The minimal steps are not class aware and simply invoke the methods to do the work of the step

